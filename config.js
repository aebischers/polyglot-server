'use strict';

var config = Object.freeze({
  NODE_ENV_PRODUCTION: 'production',
  LOG_FORMAT_PRODUCTION: 'combined',
  LOG_FORMAT_DEV: 'dev',
  LOG_FILE_DEFAULT: 'default.log',
  PUSH_API_URL: 'https://www2.aebischers.com/message'
});

module.exports = config;
