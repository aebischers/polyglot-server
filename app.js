'use strict';

const _ = require('lodash')
  , config = require('./config')
  , express = require('express')
  , morgan = require('morgan')
  , bodyParser = require('body-parser')
  , vocabularyRoute = require('./vocabulary/vocabulary.route')
  , app = express()
  , dotenv = require('dotenv').config()
  , log = require('./log/logger')
  , vocabularyService = require('./vocabulary/vocabulary.service')
  , Scheduler = require('./vocabulary/vocabulary.scheduler');

// Node Environment
const isProduction = _.isEqual(process.env.NODE_ENV, config.NODE_ENV_PRODUCTION);

// Logger configuration
const logFormat = isProduction ? config.LOG_FORMAT_PRODUCTION : config.LOG_FORMAT_DEV;
app.use(morgan(logFormat));

// Populate req.body property with incoming request body
app.use(bodyParser.json());

// Cleanup and exit
process.on('exit', function () {
  log.isDebug && log.debug("Exit process...");
  job.stop();
});

// REST Router
app.use('/vocabulary', vocabularyRoute);

// Vocabulary Scheduler
var vocabulary = vocabularyService.getSentences();
// See https://crontab.guru/#*/30_8-22_*_*_*
var job = new Scheduler('*/30 8-22 * * *', vocabulary);
job.start();

// Global Error Handler
app.use(function (err, req, res, next) {
  log.error(err);
  if (err.status === 400) {
    res.status(400).send(err);
  }
  else {
    res.status(503).send({error: "service temporarily unavailable"});
  }
});

module.exports = app;
