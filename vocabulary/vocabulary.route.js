'use strict';

const express = require('express')
  , _ = require('lodash')
  , util = require('util')
  , router = express.Router()
  , config = require('../config')
  , log = require('../log/logger')
  , vocabularyService = require('./vocabulary.service');

/* GET Sentences */
router.get('/sentences', function (req, res) {
  log.info("GET all sentences");

  var entries = vocabularyService.getSentences();
  res.send(entries);
});

module.exports = router;
