'use strict';

const _ = require('lodash')
  , util = require('util')
  , config = require('../config')
  , log = require('../log/logger')
  , xlsx = require('xlsx');


function getSentences() {
  var workbook = xlsx.readFile('./vocabulary/Englisch Vocab.xlsx');
  var worksheet = workbook.Sheets["Sentences"];

  var entries = _(worksheet).pickBy(function (value, key) {
    return _.startsWith(key, 'A') && !_.isEqual(key, 'A1');
  }).map(function (entry) {
    return entry.v;
  }).value();

  return entries;
}

module.exports = {
  getSentences: getSentences
};
