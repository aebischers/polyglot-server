'use strict';

const schedule = require('node-schedule')
  , _ = require('lodash')
  , util = require('util')
  , config = require('../config')
  , log = require('../log/logger')
  , request = require('request-promise');

function sendNotification(message) {
  log.info("Mock send message " + message);

  var options = {
    method: 'POST',
    uri: config.PUSH_API_URL,
    body: {
      title: 'Polyglot',
      body: message,
      topic: 'polyglot'
    },
    json: true
  };

  return request(options)
    .then(function (response) {
      log.info('POST message was successful', response);
    })
    .catch(function (err) {
      log.error('POST message failed', err);
    });
}

function Scheduler(cron, vocabulary) {
  this.cron = cron;
  this.vocabulary = _.shuffle(vocabulary);
  log.info(util.format('Created new scheduler with %d vocabulary entries', _.size(vocabulary)));
}

Scheduler.prototype.start = function () {
  log.info('Start scheduler');
  var that = this;
  this.job = schedule.scheduleJob(this.cron, function () {
    log.info('Running job');
    if (_.isEmpty(that.vocabulary)) {
      log.warn('Vocabulary list is empty.');
      that.stop();
      return;
    }
    var message = _.head(that.vocabulary);
    that.vocabulary = _.drop(that.vocabulary);
    sendNotification(message);
  });
};

Scheduler.prototype.stop = function () {
  log.info('Stopping scheduler job now.');
  this.job.cancel();
};

module.exports = Scheduler;
