'use strict';

var _ = require('lodash')
  , winston = require('winston')
  , config = require('../config')
  , dotenv = require('dotenv').config();

var logger = new (winston.Logger)({
  transports: [
    new winston.transports.Console({
      level: process.env.LOG_LEVEL_CONSOLE,
      handleExceptions: true,
      json: true /* Format log message as JSON */
    }),
    new winston.transports.File({
      level: process.env.LOG_LEVEL_FILE,
      name: 'file',
      filename: config.LOG_FILE_DEFAULT,
      handleExceptions: true,
      humanReadableUnhandledException: true
    })
  ]
});

logger.isDebug = _.isEqual(process.env.LOG_LEVEL_FILE, 'debug') || _.isEqual(process.env.LOG_LEVEL_CONSOLE, 'debug');
module.exports = logger;

